<?php
// header('Content-type: image/jpeg');
set_time_limit(60);
putenv('GDFONTPATH=' . realpath('.'));
include './Chart.php';
include './RetrieveData.php';
include './ModelReport.php';
use App\RetrieveData;
use App\Chart;
use App\ModelReport;

include './sendNotify.php';
//img absolute path
$path 	= "C:/xampp/htdocs/php-draw-chart-from-raw-data/"; //test
/*calculate $startDate from script active date - 7day*/
$dateNow 	= new \DateTime();
$dateFirstOfWeek = $dateNow->sub(new DateInterval("P7D"));
$dateFirstOfWeek = $dateNow->format("Y-m-d"); 

//$dataGateway = new RetrieveData("http://172.17.82.15:9876/sentry-v2/_weekly.report.php");
$dataGateway= new RetrieveData("http://localhost/php-draw-chart-from-raw-data/mockup_data.php");
$dataGet 	= $dataGateway->get();
$retry 		 = 10;
while($dataGet === false && $retry >=1){
	sleep(1);
	sendNotify(null,"\r\nAuto genarate sentry PVQ Loudness chart.\r\n Cant get data from api.\r\n Going retry.",true);
	$dataGet 	= $dataGateway->get();
	$retry--;
	if($retry == 0){
		sendNotify(null,"\r\nAuto genarate sentry PVQ Loudness chart.\r\n Cant get data from api.\r\n Over retry check api.",true);
		return false;
	}
}

$dataModel	= new ModelReport($dataGet);
$checkDataStatus = $dataModel->checkRetrieveData();

if($checkDataStatus==false){
    sendNotify(null,"Invalid data from sentry, check sentry api",true);
	return false;
}

echo "pass";
// $seriesData	= $dataModel->getByDateOffset($dateFirstOfWeek,7);
$newSeries	= $dataModel->getByDateOffset($dateFirstOfWeek,7);
$seriesData = $dataModel->updateSeries($newSeries);
/*PVQ*/
$pvqImgName	 = "pvq_".$seriesData["date"][count($seriesData["date"])-1].".png";
$chartPvq 	= new Chart();
$chartPvq->setChartType("pvq");
$chartPvq->setChartDataSeries($seriesData);
$chartPvq->createChart();
$chartPvq->render($pvqImgName);
// $chartPvq->render();
sleep(2);

/*Loudness*/
$loudnessImgName = "loudness_".$seriesData["date"][count($seriesData["date"])-1].".png";
$chartLoudness 	= new Chart();
$chartLoudness->setChartType("loudness");
$chartLoudness->setChartDataSeries($seriesData);
$chartLoudness->createChart();
$chartLoudness->render($loudnessImgName);
// $chartLoudness->render();
sleep(2);

/*QOE*/
$qoeImgName = "qoe_".$seriesData["date"][count($seriesData["date"])-1].".png";
$chartQoe 	= new Chart();
$chartQoe->setChartType("qoe");
$chartQoe->setChartDataSeries($seriesData);
$chartQoe->createChart();
$chartQoe->render($qoeImgName);
// $chartQoe->render();
sleep(2);

/*Send notify*/
sendNotify($path.$pvqImgName,$pvqImgName);
sendNotify($path.$loudnessImgName,$loudnessImgName);
sendNotify($path.$qoeImgName,$qoeImgName);
//sendNotify($path.$pvqImgName,$pvqImgName,true);
//sendNotify($path.$loudnessImgName,$loudnessImgName,true);



