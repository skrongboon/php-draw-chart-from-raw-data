<?php
namespace App;
class ModelReport
{
	private $dateStart;
	private $dateEnd;
	private $pvq;
	private $loudness;
	private $dateSeries;
	private $dataSeries;
	private $newData;

	private $currentSeries;

	private $indexDate 		= 0;
	private $indexQoe 		= 3;
	private $indexPvq 		= 5;
	private $indexLoudness 	= 7;

	public function __construct($rawData){
		/*Construct for lastest data series*/
		$raw 	= str_replace("<br>", "\n", $rawData);
		// echo $raw;
		$this->newData = str_getcsv($raw,"\n");
		// print_r($arrCsv);
		//$data 	= $this->_makeData($arrCsv);
	}

	private function _getOldSeries(){
		/* Get series from file*/
		$dbFile 	= file_get_contents('./data.json', FILE_USE_INCLUDE_PATH);
		$jsonData 	= json_decode($dbFile,1);
		return $jsonData;
	}
	private function _updateFile($lastestSeries){
		/*Update data.json 
			rename old
			create new one
		*/
		$date 	= new \Datetime();
		$dateStr= $date->format("Y-m-d");
		rename('./data.json',"./".$dateStr."_data.json");

		file_put_contents('./data.json',json_encode($lastestSeries,JSON_PRETTY_PRINT));
	}

	public function updateSeries($newSeries){
		/*  -remove first date of old series
			-add lastest serie to old series
			-return lastest series
		*/
		$oldSeries 		= $this->_getOldSeries();
		$lastestSeries	= $oldSeries ;	
		$avgSeries 		= $this->_calNewSerie($newSeries);
		// remove first
		array_shift($lastestSeries["date"]);
		array_shift($lastestSeries["qoe"]);
		array_shift($lastestSeries["pvq"]);
		array_shift($lastestSeries["loudness"]);
		// add new
		$lastestSeries["date"][] 	= $avgSeries["date"];
		$lastestSeries["pvq"][] 	= $avgSeries["pvq"];
		$lastestSeries["qoe"][] 	= $avgSeries["qoe"];
		$lastestSeries["loudness"][]= $avgSeries["loudness"];
		$this->_updateFile($lastestSeries);
		return $lastestSeries;
	}

	private function _makeData($csvArray){
		$_data 	= array();
		for ($i=0; $i < count($csvArray) ; $i++) { 
			$lineCsvArr 	= str_getcsv($csvArray[$i]);
			$_data[$lineCsvArr[$this->indexDate]] = $lineCsvArr[$this->indexPvq]; 
		}
	}

	public function getByDateOffset($dateStart,$offset){
		$dateArr 	= [];
		$pvqArr 	= [];
		$qoeArr 	= [];
		$loudnessArr= [];

		$nGet 		= 0;
		
		for ($i=0; $i < count($this->newData); $i++) { 
			$lineCsvArr	= str_getcsv($this->newData[$i]); 
			if(strpos($lineCsvArr[$this->indexDate],$dateStart) || $nGet!= 0){
				//clean date D2018-02-10 -> 2018-02-10
				$dateStr 	= str_replace("D",""
								,$lineCsvArr[$this->indexDate]);

				$dateArr[] 	= $dateStr;
				$qoeArr[] 	= $lineCsvArr[$this->indexQoe];
				$pvqArr[] 	= $lineCsvArr[$this->indexPvq];
				$loudnessArr[] = $lineCsvArr[$this->indexLoudness];
				$nGet++;
				if($nGet == $offset){
					break;
				}
			}

		}
		$result 			= [];
		$result['date'] 	= $dateArr;
		$result['pvq'] 		= $pvqArr;
		$result['qoe'] 		= $qoeArr;
		$result['loudness'] = $loudnessArr;
		return $result;
	}

	public function checkRetrieveData(){
		$checkStatus 	= false;
		$foundStart 	= false;
		$foundEnd 		= false;
		$d1 			= new \DateTime();
		$d2 			= new \DateTime();
		$dateStart 		= $d1->sub(new \DateInterval("P7D"));
		$dateStartStr 	= $dateStart->format("Y-m-d"); 
		$dateEnd 		= $d2->sub(new \DateInterval("P1D"));
		$dateEndStr 	= $dateEnd->format("Y-m-d");
		if(is_null($this->newData[0])){
			$checkStatus =  false;
		}else{
			for ($i=0; $i < count($this->newData); $i++) {
				$lineCsvArr	= str_getcsv($this->newData[$i]); 
				// check error data *sentry no data
				if($lineCsvArr[$this->indexQoe]==-40
				//||$lineCsvArr[$this->indexQoe]==0
				)
				{
					return false;
				}
				if($lineCsvArr[$this->indexPvq]==-40
				||$lineCsvArr[$this->indexPvq]==0){
					return false;
				}
				if($lineCsvArr[$this->indexLoudness]==-40
				||$lineCsvArr[$this->indexLoudness]==0){
					return false;
				}
				/*Check first date and last date */
				if(strpos($lineCsvArr[$this->indexDate],$dateStartStr)!==false){
					$foundStart =  true;
				}elseif(strpos($lineCsvArr[$this->indexDate],$dateEndStr)!==false){
					$foundEnd 	= true;
				}
			}
			if($foundStart&&$foundEnd == true){
				$checkStatus = true;
			}else echo "No data in date rangei";
		}
		return $checkStatus;
	}
	private function _calNewSerie($newSerie){
		/*AVG new serie and concat date*/
		$result 			= [];
		$result["date"] 	= "";
		$result["qoe"] 		= "";
		$result["pvq"] 		= "";
		$result["loudness"] = "";
		
		$avgQoe 			= array_sum($newSerie["qoe"])/count($newSerie["qoe"]);
		$avgPvq 			= array_sum($newSerie["pvq"])/count($newSerie["pvq"]);
		$avgLoudness		= array_sum($newSerie["loudness"])/count($newSerie["loudness"]);
		$firstDate			= new \Datetime($newSerie["date"][0]);
		$lastDate 			= new \Datetime($newSerie["date"][count($newSerie["date"])-1]);
		$newDate 			= $firstDate->format("d") . "-" . $lastDate->format("d") . " " . $lastDate->format("M");  
		$result["date"] 	= $newDate;
		$result["qoe"] 		= round($avgQoe,5);
		$result["pvq"] 		= round($avgPvq,5);
		$result["loudness"] = round($avgLoudness,5);
		return $result;
	}


}