<?php
namespace App;
	putenv('GDFONTPATH=' . realpath('.'));
    class Chart
    {
        const FONT              = 'calibri.ttf';
        const PVQ_MIN           = 4.00001;
        const PVQ_MAX           = 4.5;
        const PVQ_UNIT          = 0.05;

        const LOUDNESS_MIN      = -30;
        const LOUDNESS_MAX      = 0;
        const LOUDNESS_UNIT     = 5;
        
        const QOE_MIN           = 0;
        const QOE_MAX           = 100;
        const QOE_UNIT          = 10;

        const IMG_WIDTH         = 4267;
        const IMG_HEIGHT        = 3200;
        const IMG_BOTTOM_MARGIN = 100*7;

        const CHART_ORI_X       = 900;
        const CHART_ORI_Y       = 800;

        const CHART_WIDTH       = self::IMG_WIDTH-self::CHART_ORI_X*0.1;
        const CHART_HEIGHT      = self::IMG_HEIGHT-self::IMG_BOTTOM_MARGIN;

        const MAIN_TITLE_PVQ_X  = self::IMG_WIDTH/6.2;
        const MAIN_TITLE_PVQ_Y  = 200;
        const SUB_TITLE_PVQ_X   = self::IMG_WIDTH/2.4;
        const SUB_TITLE_PVQ_Y   = 40;

        const MAIN_TITLE_LOUDNESS_X  = self::IMG_WIDTH/3.5;
        const MAIN_TITLE_LOUDNESS_Y  = 270;
        const SUB_TITLE_LOUDNESS_X   = self::IMG_WIDTH/2.4;
        const SUB_TITLE_LOUDNESS_Y   = 40;

        const MAIN_TITLE_QOE_X  = self::IMG_WIDTH/5.7;
        const MAIN_TITLE_QOE_Y  = 200;
        const SUB_TITLE_QOE_X   = self::IMG_WIDTH/2.4;
        const SUB_TITLE_QOE_Y   = 40;

        //const CHART_LINE_HEIGHT = 30;
        //const CHART_LINE_HEIGHT_LOUDNESS = 50;
        //const CHART_BAR_WIDTH   = 20;
        //const CHART_BAR_MARGIN  = 70;
        //const CHART_BAR_FIRST_MARGIN = 60;

        const CHART_LINE_HEIGHT = 170;
        const CHART_LINE_HEIGHT_LOUDNESS = (170*10)/6;
        const CHART_BAR_WIDTH   = 90;
        const CHART_BAR_MARGIN  = 280;
        const CHART_BAR_FIRST_MARGIN = 140;


        const MARK_INDEX_PVQ    = 2; // from first-line(top-line) index == 0
        const MARK_INDEX_LOUDNESS= 4.301;
        const MARK_INDEX_QOE    = 1;// from first-line(top-line) index == 0

        const LOGO_X_POS        = self::CHART_ORI_X/2.3;
        const LOGO_Y_POS        = self::IMG_HEIGHT/2;
        const LOGO_WIDTH        = 700;
        const LOGO_HEIGHT       = 700;
        const LOGO_MSG_PVQ      = "PVQ";
        const LOGO_MSG_LOUDNESS = "LOUDNESS";
        const LOGO_MSG_QOE      = "QOE";
        const LOGO_MSG_PVQ_SIZE     = 120;
        const LOGO_MSG_LOUDNESS_SIZE= 80;
        const LOGO_MSG_QOE_SIZE     = 120;
        const LOGO_MSG_PVQ_X_POS    = self::LOGO_X_POS-140 ;
        const LOGO_MSG_LOUDNESS_X_POS= self::LOGO_X_POS-240;
        const LOGO_MSG_QOE_X_POS    = self::LOGO_X_POS-140 ;

        /*Image GD chart*/
        private $im;
        private $chartType;
        private $dateStart;
        private $mainTitle;
        private $mainTitlePosition;
        private $subTitle;
        private $subTitlePosition;
        private $yAxisTitle;
        private $yAxisTitlePosition;
        private $seriesDate;
        private $seriesValue;
        private $gray;
        private $white;
        private $black;
        private $blue;
        private $red;
        private $green;
        private $blue2;
        private $markLinePos;
        private $seriesUnit;
        private $seriesUnitMin;
        private $seriesUnitMax;
        private $lineMarkIndex;
        private $logoMsg;
        private $chartLineHeight;
        private $logoMsgSize;
        private $markValue;

        function __construct(){
            $this->im       = imagecreatetruecolor(self::IMG_WIDTH,self::IMG_HEIGHT);
            $this->gray     = imagecolorallocate($this->im, 210, 210, 210);
            $this->white    = imagecolorallocate($this->im, 255, 255, 255);
            $this->black    = imagecolorallocate($this->im, 0, 0, 0);
            $this->black2   = imagecolorallocate($this->im, 35, 35, 35);
            $this->blue     = imagecolorallocate($this->im, 0, 0, 255);
            $this->red      = imagecolorallocate($this->im, 255, 0, 0);
            $this->green    = imagecolorallocate($this->im, 0, 255, 0);
            $this->blue2    = imagecolorallocate($this->im, 70, 153, 242);


            imagefill($this->im,0,0,$this->white);
        }


        public function setChartType($type){
            $this->chartType = $type;
        }

        public function setChartDataSeries($data){
            /*
                {date:[],pvq:[],loudness:[]}
            */
            $this->seriesDate = $data['date'];
            if($this->chartType == 'pvq'){
                $this->seriesValue = $data['pvq'];
            }
            else if($this->chartType == 'qoe'){
                $this->seriesValue = $data['qoe'];
            }
            else $this->seriesValue = $data['loudness'];

        }

        public function createChart(){
            $this->_InitChart();
            $this->_drawChartTitle();
            $this->_drawChartTemplate();
            $this->_drawChartSeries();
            $this->_drawChartMarkLine();
            $this->_drawChartLogo();
        }
        public function render($filename = null){
            if($filename == null){
                header('Content-type: image/jpeg');
            }
            imagepng($this->im,$filename);
            imagedestroy($this->im);
        }

        private function _createSubtitle(){
            $startDate  = new \DateTime($this->seriesDate[0]);
            $starEnd    = new \DateTime($this->seriesDate[count($this->seriesDate)-1]);
            return $startDate->format("d-M")." to "
                .$starEnd->format("d-M")
                ." ".$startDate->format("Y");
        }

        private function _InitChart(){
            /*
                set position,title
            */
            $this->yAxisTitlePosition   = [self::CHART_ORI_X-300,self::CHART_ORI_Y-150];
            if($this->chartType == "pvq"){
                $this->mainTitle        = "Average PVQ : Perceptual Video Quality";
                $this->mainTitlePosition= [self::MAIN_TITLE_PVQ_X,self::MAIN_TITLE_PVQ_Y];
                $this->subTitlePosition = [self::SUB_TITLE_PVQ_X,self::SUB_TITLE_PVQ_Y];
                $this->yAxisTitle       = "Average PVQ";
                $this->seriesUnit       = self::PVQ_UNIT;
                $this->seriesUnitMin    = self::PVQ_MIN;
                $this->seriesUnitMax    = self::PVQ_MAX;
                $this->lineMarkIndex    = self::MARK_INDEX_PVQ;
                $this->logoMsg          = self::LOGO_MSG_PVQ;
                $this->chartLineHeight  = self::CHART_LINE_HEIGHT;
                $this->logoMsgSize      = self::LOGO_MSG_PVQ_SIZE;
                $this->logoMsgXPos      = self::LOGO_MSG_PVQ_X_POS;
            }elseif($this->chartType == "loudness"){
                $this->mainTitle        = "Average Loudness Level";
                $this->mainTitlePosition= [self::MAIN_TITLE_LOUDNESS_X,self::MAIN_TITLE_LOUDNESS_Y];
                $this->subTitlePosition = [self::SUB_TITLE_LOUDNESS_X,self::SUB_TITLE_LOUDNESS_Y];
                $this->yAxisTitle       = "Average Loudness Level (dB LKFS)";
                $this->seriesUnit       = self::LOUDNESS_UNIT;
                $this->seriesUnitMin    = self::LOUDNESS_MIN;
                $this->seriesUnitMax    = self::LOUDNESS_MAX;
                $this->lineMarkIndex    = self::MARK_INDEX_LOUDNESS;
                $this->logoMsg          = self::LOGO_MSG_LOUDNESS;
                $this->chartLineHeight  = self::CHART_LINE_HEIGHT_LOUDNESS;
                $this->logoMsgSize      = self::LOGO_MSG_LOUDNESS_SIZE;
                $this->logoMsgXPos      = self::LOGO_MSG_LOUDNESS_X_POS;
            }elseif($this->chartType == "qoe"){
                $this->mainTitle        = "Average QOE : Quality Of Experience";
                $this->mainTitlePosition= [self::MAIN_TITLE_QOE_X,self::MAIN_TITLE_QOE_Y];
                $this->subTitlePosition = [self::SUB_TITLE_QOE_X,self::SUB_TITLE_QOE_Y];
                $this->yAxisTitle       = "Average QOE";
                $this->seriesUnit       = self::QOE_UNIT;
                $this->seriesUnitMin    = self::QOE_MIN;
                $this->seriesUnitMax    = self::QOE_MAX;
                $this->lineMarkIndex    = self::MARK_INDEX_QOE;
                $this->logoMsg          = self::LOGO_MSG_QOE;
                $this->chartLineHeight  = self::CHART_LINE_HEIGHT;
                $this->logoMsgSize      = self::LOGO_MSG_QOE_SIZE;
                $this->logoMsgXPos      = self::LOGO_MSG_QOE_X_POS;
            }
            // $this->subTitle             = $this->_createSubtitle();
        }

        private function _drawChartTitle(){
            //draw title
            imagefttext($this->im
                , 140, 0
                ,$this->mainTitlePosition[0]
                ,$this->mainTitlePosition[1]
                , $this->black, self::FONT, $this->mainTitle);
            //draw date              
            // imagestring($this->im,2
            //     ,$this->subTitlePosition[0]
            //     ,$this->subTitlePosition[1]
            //     ,$this->subTitle, $this->black);
            //draw y axis name               
            //imagestring($this->im,3
            //    ,$this->yAxisTitlePosition[0]
            //    ,$this->yAxisTitlePosition[1]
            //    ,$this->yAxisTitle,$this->black);
            imagefttext($this->im
                , 70, 0
                ,$this->yAxisTitlePosition[0]
                ,$this->yAxisTitlePosition[1]
                , $this->black, self::FONT, $this->yAxisTitle);

        }

        private function _drawChartTemplate(){
            // $style  = array($this->white,$this->white,$this->gray,$this->white,$this->white);
            // imagesetstyle($this->im,$style);
            $unit       = $this->seriesUnit;
            //$unitMax    = $this->seriesUnitMax;
            $currentUnitValue   = $this->seriesUnitMax;
            $unitMin    = $this->seriesUnitMin;
            $lineCount  = 0;
            $markLineIndex  = $this->lineMarkIndex; 
            /*cast float to integer*/
            $cur = (int)($currentUnitValue*100);
            $min = (int)$unitMin*100;
            $u = (int)($unit*100);
            while($cur>=$min){
                $lineYPos     = self::CHART_ORI_Y+($lineCount*$this->chartLineHeight);
                //imageline($this->im
                //    ,self::CHART_ORI_X
                //    ,$lineYPos
                //    ,self::CHART_WIDTH
                //    ,$lineYPos,$this->gray);
                imagefilledrectangle($this->im
                    ,self::CHART_ORI_X
                    ,$lineYPos
                    ,self::CHART_WIDTH
                    ,$lineYPos+5,$this->gray);
                //imagestring($this->im,5
                //    ,self::CHART_ORI_X-40
                //    ,$lineYPos-7
                //    ,$currentUnitValue, $this->black);
                if($lineCount == $markLineIndex){
                    $this->markLinePos["x1"] = self::CHART_ORI_X;
                    $this->markLinePos["y1"] = $lineYPos;
                    $this->markLinePos["x2"] = self::CHART_WIDTH;
                    $this->markLinePos["y2"] = $lineYPos;
                    $this->markValue = $currentUnitValue;
                }else{
                    imagefttext($this->im
                        , 38, 0
                        , self::CHART_ORI_X-100
                        , $lineYPos+10 
                        , $this->black, self::FONT, $currentUnitValue);
                }
                // if($this->chartType == "loudness" && $currentUnitValue>$unitMin){
                //     $this->_drawSmallLine($lineCount,$currentUnitValue,$lineYPos);
                // }
               
                if((int)$cur>(int)$min){
                    $this->_drawSmallLine($lineCount,$currentUnitValue,$lineYPos);
                }
                $currentUnitValue  = $currentUnitValue-$unit;
                $cur = $cur-$u;
                $lineCount++;
            }
        }
        private function _drawSmallLine($lineCount,$currentUnitValue,$lineYPos){
            $style  = array($this->white,$this->gray,$this->white);
            imagesetstyle($this->im,$style);
            $smallLineHeight= $this->chartLineHeight/5;
            $markValue  = $currentUnitValue;
            for ($i=0; $i < 4; $i++) { 
                $markValue--;
                $smallLineYPos  = $lineYPos+$smallLineHeight+($i*$smallLineHeight);
                imageline($this->im
                ,self::CHART_ORI_X
                ,$smallLineYPos
                ,self::CHART_WIDTH
                ,$smallLineYPos,IMG_COLOR_STYLED);
                $lineCount += 0.100;
                if($this->chartType == "loudness"){
                    //if($lineCount == $this->lineMarkIndex){
                    if(abs($lineCount-$this->lineMarkIndex)<0.01){
                        $this->markLinePos["x1"] = self::CHART_ORI_X;
                        $this->markLinePos["y1"] = $smallLineYPos;
                        $this->markLinePos["x2"] = self::CHART_WIDTH;
                        $this->markLinePos["y2"] = $smallLineYPos;
                        $this->markValue = $markValue;
                        // draw mark line label
                        //imagestring($this->im,3
                        //    ,self::CHART_ORI_X-30
                        //    ,$smallLineYPos-7
                        //    ,$markValue, $this->red);

                    }
                }
            }
        }
        private function _drawChartSeries(){
            //Bar
            $barWidth           = self::CHART_BAR_WIDTH;
            $barMargin          = self::CHART_BAR_MARGIN;
            $firstBarLeftMargin = self::CHART_BAR_FIRST_MARGIN; 
            $barStartX          = self::CHART_ORI_X+$firstBarLeftMargin;
            $barStartY          = self::CHART_HEIGHT; //from chart height คงที่
            $maxHeight          = self::CHART_HEIGHT-self::CHART_ORI_Y;
         
            //draw bar 
            for ($i=0; $i < count($this->seriesValue); $i++) { 
                //draw bar
                $x1         = $barStartX+($i*$barMargin);
                $x2         = $x1;
                $value      = $this->seriesValue[$i];
                $date       = $this->seriesDate[$i];
                $yTop       = $this->_calBarHeight($maxHeight,$value);
                imagesetthickness($this->im, $barWidth);
                imageline($this->im
                    ,$x1,$barStartY
                    ,$x2,$yTop
                    ,$this->blue2);

                //draw value label
                //imagestring($this->im
                //    ,2,$x2-25,$yTop-20
                //    ,$value,$this->black);
                imagefttext($this->im
                    , 40, 0
                    , $x2-80
                    , $yTop-30
                    , $this->black, self::FONT
                    , $value);
                //draw date
                //imagestring($this->im
                //    ,2,$x1-20,$barStartY+10
                //    // ,$this->_formatDate($date),$this->black);
                //    ,$date,$this->black);
                imagefttext($this->im
                    , 40, 0
                    ,$x1-65
                    ,$barStartY+60
                    , $this->black, self::FONT
                    , $this->_formatDate($date));
            }
        }

        private function _drawChartMarkLine(){
            //imagesetthickness($this->im, 3);
            //imageline($this->im
            //    ,$this->markLinePos['x1'],$this->markLinePos['y1']
            //    ,$this->markLinePos['x2'],$this->markLinePos['y2']
            //    ,$this->red);
            //imagesetthickness($this->im, 1);
            // Line
            imagefilledrectangle($this->im
                ,$this->markLinePos['x1']
                ,$this->markLinePos['y1']-5
                ,$this->markLinePos['x2']
                ,$this->markLinePos['y2']+5
                ,$this->red);
            //Label
            imagefttext($this->im
                , 45, 0
                , self::CHART_ORI_X-100
                , $this->markLinePos['y1']+10
                , $this->red, self::FONT, $this->markValue);
        }

        private function _drawChartLogo(){
            $font = 'calibri.ttf';

            // Add the text
            imagefilledellipse($this->im
                , self::LOGO_X_POS, self::LOGO_Y_POS
                , self::LOGO_WIDTH, self::LOGO_HEIGHT, $this->black2);
            imagefilledellipse($this->im
                , self::LOGO_X_POS, self::LOGO_Y_POS
                , self::LOGO_WIDTH-30, self::LOGO_HEIGHT-30, $this->white);
            imagefilledellipse($this->im
                , self::LOGO_X_POS, self::LOGO_Y_POS
                , self::LOGO_WIDTH-60, self::LOGO_HEIGHT-60, $this->black2);
            //imageellipse($this->im, self::LOGO_X_POS, self::LOGO_Y_POS, 100, 100, $this->white);
            //imageellipse($this->im, self::LOGO_X_POS, self::LOGO_Y_POS, 102, 102, $this->white);
            //imageellipse($this->im, self::LOGO_X_POS, self::LOGO_Y_POS, 104, 104, $this->white);
            imagettftext($this->im, $this->logoMsgSize, 0
                , $this->logoMsgXPos, self::LOGO_Y_POS+40
                , $this->white, $font, $this->logoMsg);
        }

        private function _formatDate($dateStr){
            // $datestr    = str_replace("d","",$datestr);
            //$date       = new \DateTime($dateStr,new \DateTimeZone("Asia/Bangkok"));
            //return $date->format("d-M");
            $datestr    = str_replace(" ","\n  ",$dateStr);
            return $datestr;

        }

        private function _calBarHeight($maxHeight,$barValue){
            //maxHeight = chart height =300
            if($this->chartType == 'pvq'){
                $valueFix   = 4 ; // for pvq reset min/max 4-4.5 to 0-0.5
                $barValue   = $barValue-$valueFix;
                $pvqMax     = self::PVQ_MAX-$valueFix;
                $valuePercent = ($barValue*100)/$pvqMax;
                $height = ($valuePercent*$maxHeight)/100; 
                $height = $maxHeight-$height+self::CHART_ORI_Y;
            }else if($this->chartType == 'loudness'){
                $barValue   = $barValue*-1;
                $valuePercent = ($barValue*100)/(self::LOUDNESS_MIN*-1);
                $height = ($valuePercent*$maxHeight)/100; 
                $height = $height+self::CHART_ORI_Y;
            }elseif($this->chartType == 'qoe'){
                $valueFix   = 0 ; 
                $barValue   = $barValue-$valueFix;
                $qoeMax     = self::QOE_MAX-$valueFix;
                $valuePercent = ($barValue*100)/$qoeMax;
                $height = ($valuePercent*$maxHeight)/100; 
                $height = $maxHeight-$height+self::CHART_ORI_Y;
            }

            return $height; //y2 pos

        }

    }
?>